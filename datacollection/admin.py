from django import forms
from django.db.models import get_models
from django.contrib import admin

from localflavor.ca.forms import (
    CAPhoneNumberField,
    CAPostalCodeField,
    CAProvinceField,
    CAProvinceSelect,
)

import datacollection
from datacollection.models import (
    FamilyDoctor,
    Location,
)


class FamilyDoctorForm(forms.ModelForm):
    phone = CAPhoneNumberField()
    fax = CAPhoneNumberField()


class FamilyDoctorAdmin(admin.ModelAdmin):
    form = FamilyDoctorForm


admin.site.register(FamilyDoctor, FamilyDoctorAdmin)


class LocationForm(forms.ModelForm):
    province = CAProvinceField(widget=CAProvinceSelect)
    postal_code = CAPostalCodeField()


class LocationAdmin(admin.ModelAdmin):
    form = LocationForm


admin.site.register(Location, LocationAdmin)


for model in get_models(datacollection.models):
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
