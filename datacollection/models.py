from django.db import models


class Pharmacy(models.Model):
    """
    """
    name = models.CharField(max_length=200)


class FamilyDoctor(models.Model):
    """
    """
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    fax = models.CharField(max_length=12)


class Ethnicity(models.Model):
    """
    """
    name = models.CharField(max_length=100)


class Person(models.Model):
    """Person participating in the study.
    """
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    dob = models.DateField()
    @property
    def bmi(self):
        """The weight is spected to be in kg, height in m."
        """
        return (self.weight / (self.height * self.height))

    family_doctor = models.ForeignKey(FamilyDoctor, null=True)
    regular_pharmacy = models.ForeignKey(Pharmacy)

    # XXX: What about parents belonging to multiple ethnic groups?
    father_ethnicity = models.ForeignKey(Ethnicity, related_name='+')
    mother_ethnicity = models.ForeignKey(Ethnicity, related_name='+')


class HIN(models.Model):
    """Storing the HINs in a protected manner.
    """
    person = models.ForeignKey(Person)
    # TODO: Use OpenPGP to store HIN
    hin = models.CharField(max_length=20)


class Location(models.Model):
    """Location where walk-in sessions are held.
    """
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=50)
    province = models.CharField(max_length=2)
    postal_code = models.CharField(max_length=7)

    class Meta:
        unique_together = (
            ('name', 'city'),
        )


class Encounter(models.Model):
    """A walk-in visit by a person participating in the study.

    On every visit the following data is gathered:

    - Blood pressure
    - Medication list
    - Prevention

    Every six months:

    - Risk assessment
    """
    date = models.DateField()
    location = models.ForeignKey(Location)
    visitor = models.ForeignKey(Person)

    class Meta:
        unique_together = (
            ('date', 'visitor'),
        )


class EncounterNote(models.Model):
    """A note taken in an encounter.
    """
    encounter = models.ForeignKey(Encounter)
    # TODO: Use markdown/textile
    note = models.TextField(blank=True)

    class Meta:
        unique_together = (
            ('encounter'),
        )


class BloodPressureMeasure(models.Model):
    """
    """
    visit = models.ForeignKey(Encounter)
    systolic = models.PositiveSmallIntegerField()
    diastolic = models.PositiveSmallIntegerField()
    pulse = models.PositiveSmallIntegerField()


class RiskAssessment(models.Model):
    """A risk assessment made to a person.

    The questionaire is a combination of CANRISK and the CHAP profile
    forms.

    The assessment should be conducted every six months.
    """
    visit = models.ForeignKey(Encounter)
    height_measure = models.IntegerField()
    height_metric = models.CharField(max_length=3)
    weight_measure = models.IntegerField()
    weight_metric = models.CharField(max_length=3)


class MedicationList(models.Model):
    """List of medications at a certain moment in time.

    XXX: Need a way to signal that no medication is being taken.
    """
    pass
